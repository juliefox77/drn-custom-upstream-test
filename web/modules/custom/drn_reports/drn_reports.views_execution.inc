<?php

use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\user\Entity\User;

/**
 * Implements hook_views_query_alter().
 */
function drn_reports_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
	/*
		----- IMPORTANT NOTES: ----
	  	
	  	1) having debug statements in here can cause the Preview functionality not to work in the View Builder
	  	2) if you want to use addWhere and refernce a field, that field must already be in the view builder's
	  		filter criteria. So, add it and set the value to IS NOT NULL
		3) The views_query_alter funciton must be in the views_execution.inc file; not the .module file
  	*/


	//print "DEBUG: drn_reports_views_query_alter<br/>Current view id = " . $view->id() . "<br/><br/>";


	// For specific views, manipulate the query programatically
	switch($view->id()){

	  	/* ------ START: DRC Church Roster ------ */
	  	case "drc_church_roster":
	  	case "drc_user_interests":
	  		$org_id = NULL;
	  		//print_r ($query); // do not run this - it will give memory allocation error
	  		//print "<br>---- Original Where Clause: <br/>";
	  		//print_r($query->where); //just print the where clause - this works!

			$user = \Drupal::currentUser();
			if($user){
				$uid = $user->id();


				$org_id = drn_users_get_user_org_id_for_user($uid);

	   	  		if($org_id){
		   	  		//make sure these fields are also added to the filter criteria in the view builder, even if it's just set to IS NOT NULL
		   	  		//otheriwse you will get errors!!!
					$query->addWhere('conditions','user__field_user_organization.field_user_organization_target_id',$org_id,'=');

		  		} else {
					$query->addWhere('conditions', TRUE, FALSE, '=');  //Force to No Results Found if user does not have a Church selected
		  		}

	  			//print "<br><br>---- New Where Clause using org id: " . $org_id . "<br/>";
		  	    //print_r($query->where); //print again, after manipulating query
			}

	  	    break;
	  	/* ------ END: DRC Church Roster ------ */

	  	/* ------ START: DRC Church Training AND DRC Church Training By Course, DRC Church Training By Date ------ */
	  	case "drc_church_training_by_user": /* by user */
	  	case "drc_church_training_by_course": /* by course */
	  	case "drc_church_training_by_date": /* by date */
	  		$org_id = NULL;
	  		//print_r ($query); // do not run this - it will give memory allocation error
	  		//print "<br>---- Original Where Clause: <br/>";
	  		//print_r($query->where); //just print the where clause - this works!


			$user = \Drupal::currentUser();
			if($user){
				$uid = $user->id();


				$org_id = drn_users_get_user_org_id_for_user($uid);

				//print_r($user);

			    //if($user->field_user_organization){
			    //	$variables['user_org_2'] = $user->field_user_organization->view();
			    //}

	   	  		if($org_id){
		   	  		//make sure these fields are also added to the filter criteria in the view builder, even if it's just set to IS NOT NULL
		   	  		//otheriwse you will get errors!!!
					$query->addWhere('conditions','users_field_data_opigno_learning_path_achievements__user__field_user_organization.field_user_organization_target_id',$org_id,'=');

		  		} else {
					$query->addWhere('conditions', TRUE, FALSE, '=');  //Force to No Results Found if user does not have a Church selected
		  		}

		  		//print "<br><br>---- New Where Clause using org id: " . $org_id . "<br/>";
			  	//print_r($query->where); //print again, after manipulating query

			}

	  	    break;
	  	/* ------ END: DRC Church Training ------ */

		default:
	  	 	break;
	} //END: switch
} //END: drn_reports_views_query_alter

?>