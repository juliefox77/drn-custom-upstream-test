<?php

namespace Drupal\drn_users\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\user\Entity\User;


class HomePageRedirect implements EventSubscriberInterface {

/**
* {@inheritdoc}
*/

  /* ======================================================= */
  /* This function handles redirecting users away from the 
     home page. Their destination depends on their role      */
  /* ======================================================= */
public function checkFrontRedirection(GetResponseEvent $event) {
  //print "<br>DEBUG 18: checking if on front page";
  
  $currentURL = \Drupal::request()->server->get('REQUEST_URI', NULL);

  // Redirects /uuid/{uuid} to the corresponding user dashboard
  if (str_starts_with($currentURL, "/uuid/")) {
      $user = \Drupal::currentUser();
      $uuid = basename($currentURL);

      if ($user->hasPermission('access_uuid')) {
        if (is_string($uuid) && (preg_match('/^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}$/', $uuid) === 1)) {
          $uid = db_query("SELECT uid FROM users WHERE uuid = :uuid", array(':uuid' => $uuid))->fetchField();
          
          if ($uid) {
            $response = new RedirectResponse('/user/' . $uid);
            $response->send();
            return;
          }
        }
      } else {
        $response = new RedirectResponse('/no-access');
        $response->send();
        return;
      }
  }
}

/**
* {@inheritdoc}
*/
public static function getSubscribedEvents() {
  $events[KernelEvents::REQUEST][] = array('checkFrontRedirection');
  return $events;
 }

}
