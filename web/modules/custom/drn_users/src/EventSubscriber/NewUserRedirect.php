<?php

namespace Drupal\drn_users\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\views\Views;
use Drupal\views\ViewExecutable;
use Drupal\user\Entity\User;
use Drupal\Core\Url;

/**
 * Class NewUserRedirect.
 */
class NewUserRedirect implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */

  /* ======================================================= */
  /* This function basically locks the user out of amlost the 
     entire site until they have filled out their user profile
     in it's entirety.                                       */
  /* ======================================================= */
  public function whitelistRedirect(GetResponseEvent $event) {
    $userID = \Drupal::currentUser()->id();
    $user = User::load($userID);
    $userRoles = $user->getRoles();

    //print "<br>DEBUG 27: In whitelistRedirect. UserID = " . $userID;    

    //if user is not_live
    if (in_array('not_live', $userRoles)) {
        user_logout();
        $event->setResponse(new RedirectResponse('/no-access', 302));

    //if user is new_user
    } elseif (in_array('new_user', $userRoles)) {
        //print "<br>DEBUG 31: has new_user role";

        $request = \Drupal::request();

        // If request is not AJAX (the opigno trainings use ajax so we need this)
        if (!$request->isXmlHttpRequest()) {
          $uid = $user->get('uid')->value;
          $requestURL = \Drupal::request()->server->get('REQUEST_URI', NULL); //current page's url


          //URLs that New Users ARE allowed to go to:
          $userEditPage = '/user/' . $userID . '/edit';
          //$userChangePassword = '/user/' . $userID . '/change-password';
          //$userChangePasswordAlias = '/user/change-password';
          $userChangePassword = '/user/' . $userID . '/password';
          $userChangePasswordAlias = '/user/password';


          // If page is not on the whitelist
          if (strpos($requestURL, $userEditPage) === false 
            && strpos($requestURL, $userChangePassword) === false 
            && strpos($requestURL, $userChangePasswordAlias)  === false 
            && strpos($requestURL, 'user/logout') === false
            ) {
            //print "<br>DEBUG 49: " . " requestURL = " . $requestURL . " redirecting to " . $userEditPage;
            $event->setResponse(new RedirectResponse($userEditPage, 302));
            \Drupal::messenger()->addError(t('NOTICE: You must fill out ALL required fields on this page in order to gain access to the rest of the site.'));
            /*\Drupal::messenger()->addWarning(t('If you just reset your password you can still continue filling out your User Profile on this page.<br/>
              After saving your User Profile, check your email and click on the Password Reset link to finish resetting your password'));*/

          } //END: if $requestURL not specific pages
        } //END: if request is not AJAX    
      } //END: if user role
  } //END: whitelistRedirect function

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('whitelistRedirect');
    return $events;
  }
}
