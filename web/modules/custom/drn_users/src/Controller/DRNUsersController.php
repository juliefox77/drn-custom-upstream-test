<?php

namespace Drupal\drn_users\Controller;

use Drupal\Core\Controller\ControllerBase;

// Change following https://www.drupal.org/node/2457593
// See https://www.drupal.org/node/2549395 for deprecate methods information
// use Drupal\Component\Utility\SafeMarkup;
use Drupal\Component\Utility\Html;
// use Html instead SAfeMarkup

/**
 * Controller routines for Lorem ipsum pages.
 */
class DRNUsersController extends ControllerBase {

  /**
   * Constructs Lorem ipsum text with arguments.
   * This callback is mapped to the path
   * 'loremipsum/generate/{paragraphs}/{phrases}'.
   * 
   */
  public function content() {
    return [
      '#theme' => 'drn_users_theme_hook',
    ];
  }
}
