<?php

/**
 * @file
 * Contains \Drupal\drn_users\Form\PrintBadgeForm.
 */

namespace Drupal\drn_users\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Credit form.
 */
class PrintBadgeForm extends FormBase {
    /**
    * {@inheritdoc}
    */
    public function getFormId() {
        return 'print_badge_form';
    }

    /**
    * {@inheritdoc}
    */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $form['user'] = array(
            '#type' => 'hidden',
            '#required' => TRUE,
        );

        $form['notes'] = array(
            '#type' => 'textfield',
            '#title' => t('Badge Printing Notes (optional)'),
        );
        
        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Submit'),
        );

        // Set default values from query strings
        $uid = \Drupal::request()->query->get('uid');
        $reprint = \Drupal::request()->query->get('reprint');
        if ($uid) {
            $form["user"]["#default_value"] = $uid;
        }
        if ($reprint) {
            $form["notes"]["#title"] = t('Reason for Reprint Request (optional)');
        }

        return $form;
    }

    /**
    * {@inheritdoc}
    */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $user_id = $form_state->getValue('user');
        $adminUser = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        $adminString = $adminUser->field_user_first_name->value . " " . $adminUser->field_user_last_name->value . " (" . $adminUser->uid->value . ")";
        $notes = $form_state->getValue('notes');
        $reprint = \Drupal::request()->query->get('reprint');
        
        try {
            $database = \Drupal::database();
            $account = \Drupal\user\Entity\User::load($user_id);
            $name = $account->field_user_first_name->value . " " . $account->field_user_last_name->value;
            $rr_complete_string = NULL;
            foreach ($account->field_response_roles_complete->referencedEntities() as $rr_complete){
                if($rr_complete_string === NULL){
                    // We are on the first one in the loop so do not put a comma
                    $rr_complete_string = $rr_complete->getName();
                } else {
                    $rr_complete_string .= "," . $rr_complete->getName();
                }
            }
            
            if ($reprint) {
                $account->set('field_badge_printed', 0);
                $violations = $account->validate();
                if (count($violations) === 0) {
                    $account->save();
                }

                \Drupal::messenger()->addMessage(t("Badge for @user has been marked for reprint.", array("@user" => $name)));

                $reprintString = "Badge Reprint Requested with Response Roles: " . $rr_complete_string . " Notes: " . $notes;
                $database->query("call deactivate_badge($user_id, '$adminString', '$reprintString');");
            } else {
                $account->set('field_badge_printed', 1);
                $violations = $account->validate();
                if (count($violations) === 0) {
                    $account->save();
                }
                
                \Drupal::messenger()->addMessage(t("Badge for @user has been marked as printed.", array("@user" => $name)));

                $printString = "Badge Printed with Response Roles: " . $rr_complete_string;
                $adminPrintString = "Printed By: " . $adminString . " Notes: " . $notes;
                $database->query("call queue_message($user_id, '$printString', '$adminPrintString', 'LOGONLY', '');");
            }

        }
        catch(Exception $e) {
            \Drupal::logger('drn_users')->error($e->getMessage());
            \Drupal::messenger()->addMessage(t("There was a problem with your request"));
        }
        
        $url = Url::fromUri('internal:/user/' . $user_id . '/edit#edit-group-user-badge-info');
        $form_state->setRedirectUrl($url);
    }
}
