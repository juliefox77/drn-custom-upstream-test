<?php

namespace Drupal\drn_users\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'DRN User Dashboard' block.
 *
 * @Block(
 *   id = "drn_user_dashboard_block",
 *   admin_label = @Translation("TEST DRN User Dashboard"),
 *
 * )
 */
class DRNUserDashboard extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
  // do something
    return array(
      '#title' => 'TEST DRN User Dashboard',
      '#description' => 'Block to display User Dashboard',
      '#markup' => $this->t('This is the content for the User Dashboard Block'),
    );
  }
}

?>