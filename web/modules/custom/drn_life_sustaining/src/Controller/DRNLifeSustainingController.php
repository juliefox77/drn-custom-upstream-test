<?php

namespace Drupal\drn_life_sustaining\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Utility\Html;

class DRNLifeSustainingController extends ControllerBase {

  public function content() {
    $gid = \Drupal::routeMatch()->getParameter('gid');
    $group = \Drupal\group\Entity\Group::load($gid);
    $database = \Drupal::database();

    $org_name = $group->label->value;

    // Query database for lsc values
    $query = $database->select('udrn_ls_org_summary', 'lsc')
    ->condition('lsc.org_id', $gid, '=')
    ->fields('lsc', ['current_level', 'l1_pct', 'l2_pct', 'l3_pct', 'l4_pct', 'l5_pct']);
    $result = $query->execute();
    $record = $result->fetchObject();
    $org_lsc_level = $record->current_level;

    $query = $database->select('udrn_ls_index', 'i');
    $query->leftJoin('udrn_ls_org_details', 'd', 'i.ls_index_id = d.ls_index_id');
    $query->fields('i', ['level', 'section', 'display_label'])
    ->fields('d', ['status', 'notes'])
    ->condition('d.org_id', $gid)
    ->orderBy('i.sort_order', 'ASC');
    $result = $query->execute();
    $org_lsc_data = [];
    foreach ($result as $r) {
      $org_lsc_data[$r->level][$r->section][] = array("name" => $r->display_label, "checked" => $r->status, "notes" => $r->notes);
    }

    return array(
      '#theme' => 'drn_life_sustaining',
      '#org_name' => $org_name,
      '#org_lsc_level' => $org_lsc_level,
      '#org_lsc_data' => $org_lsc_data,
    );
  }

}