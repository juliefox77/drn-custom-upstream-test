<?php

namespace Drupal\drn_i_am_ready\Controller;

use Drupal\Core\Controller\ControllerBase;

// Change following https://www.drupal.org/node/2457593
// See https://www.drupal.org/node/2549395 for deprecate methods information
// use Drupal\Component\Utility\SafeMarkup;
use Drupal\Component\Utility\Html;
// use Html instead SAfeMarkup

/**
 * Controller routines for Lorem ipsum pages.
 */
class DRNIAMReadyController extends ControllerBase {

  /**
   * Constructs Lorem ipsum text with arguments.
   * This callback is mapped to the path
   * 'loremipsum/generate/{paragraphs}/{phrases}'.
   * 
   */
  public function content() {
    $uid = \Drupal::routeMatch()->getParameter('uid');
    $user = \Drupal\user\Entity\User::load($uid);
    $current_user = \Drupal::currentUser();
    $current_uid = $current_user->id();
    $perm_edit_iar_page = FALSE;
    $perm_view_iar_page = FALSE;
    $page_user_name = "User Name";

    // If current user is looking at own dashboard or user has iar page access permissions
    if ($uid == $current_uid || $current_user->hasPermission('edit_iar_pages')) {
      $perm_edit_iar_page = TRUE;
    }

    // If current user has view iar page role
    if ($current_user->hasPermission('view_iar_pages')) {
      $perm_view_iar_page = TRUE;
    }

    // Get page user name
    if ($user->field_user_first_name->value || $user->field_user_last_name->value) {
      $page_user_name = $user->field_user_first_name->value . " " . $user->field_user_last_name->value;
    }

    // Get current IAR Level
    if($user->field_user_iar_level->entity){
      $iar_level = $user->field_user_iar_level->entity->getName();
    }

    // Supplies and Certifications forms
    $iar_level_2_supplies_form = \Drupal::entityTypeManager()
      ->getFormObject('user', 'iar_level_2')
      ->setEntity($user);
    $iar_level_3_supplies_form = \Drupal::entityTypeManager()
      ->getFormObject('user', 'iar_level_3')
      ->setEntity($user);
    $iar_level_4_supplies_form = \Drupal::entityTypeManager()
      ->getFormObject('user', 'iar_level_4')
      ->setEntity($user);
    $iar_level_3_app_form = \Drupal::entityTypeManager()
      ->getFormObject('user', 'iar_level_3_app')
      ->setEntity($user);
    $iar_level_4_app_form = \Drupal::entityTypeManager()
      ->getFormObject('user', 'iar_level_4_app')
      ->setEntity($user);
    $iar_level_5_app_form = \Drupal::entityTypeManager()
      ->getFormObject('user', 'iar_level_5_app')
      ->setEntity($user);

    return [
      '#title' => 'I AM Ready™ Program',
      '#description' => 'Block to display current user\'s I AM Ready Badge Level.',
      '#theme' => 'drn_i_am_ready',
      '#perm_edit_iar_page' => $perm_edit_iar_page,
      '#perm_view_iar_page' => $perm_view_iar_page,
      '#page_user_name' => $page_user_name,
      '#iar_level' => $iar_level,
      '#iar_level_one_add' => \Drupal::service('i_am_ready_reqs')->calculateIARReqs($user, 'iar_level_1_add', 'field_user_iar_level_1_add'),
      '#iar_level_one_courses' => \Drupal::service('i_am_ready_reqs')->calculateIARReqs($user, 'iar_level_1_courses', 'field_user_iar_level_1_courses'),
      '#iar_level_two_add' => \Drupal::service('i_am_ready_reqs')->calculateIARReqs($user, 'iar_level_2_add', 'field_user_iar_level_2_add'),
      '#iar_level_two_courses' => \Drupal::service('i_am_ready_reqs')->calculateIARReqs($user, 'iar_level_2_courses', 'field_user_iar_level_2_courses'),
      '#iar_level_three_add' => \Drupal::service('i_am_ready_reqs')->calculateIARReqs($user, 'iar_level_3_add', 'field_user_iar_level_3_add'),
      '#iar_level_three_courses' => \Drupal::service('i_am_ready_reqs')->calculateIARReqs($user, 'iar_level_3_courses', 'field_user_iar_level_3_courses'),
      '#iar_level_four_add' => \Drupal::service('i_am_ready_reqs')->calculateIARReqs($user, 'iar_level_4_add', 'field_user_iar_level_4_add'),
      '#iar_level_four_courses' => \Drupal::service('i_am_ready_reqs')->calculateIARReqs($user, 'iar_level_4_courses', 'field_user_iar_level_4_courses'),
      '#iar_level_five_add' => \Drupal::service('i_am_ready_reqs')->calculateIARReqs($user, 'iar_level_5_add', 'field_user_iar_level_5_add'),
      '#iar_level_five_courses' => \Drupal::service('i_am_ready_reqs')->calculateIARReqs($user, 'iar_level_5_courses', 'field_user_iar_level_5_courses'),
      '#form_level_two_supplies' => \Drupal::formBuilder()->getForm($iar_level_2_supplies_form),
      '#form_level_three_supplies' => \Drupal::formBuilder()->getForm($iar_level_3_supplies_form),
      '#form_level_four_supplies' => \Drupal::formBuilder()->getForm($iar_level_4_supplies_form),
      '#form_level_three_app' => \Drupal::formBuilder()->getForm($iar_level_3_app_form),
      '#form_level_four_app' => \Drupal::formBuilder()->getForm($iar_level_4_app_form),
      '#form_level_five_app' => \Drupal::formBuilder()->getForm($iar_level_5_app_form),
    ];
  }
}
