<?php

// The namespace is Drupal\[module_key]\[Directory\Path(s)]
namespace Drupal\drn_i_am_ready\Service;

/**
 * The DoStuff service. Does a bunch of stuff.
 */
class IAMReadyReqs {

  /**
   * Does something.
   *
   * @return string
   *   Some value.
   */
  public function calculateIARReqs(object $user, string $taxId, string $valuesId) {
    
    $reqsList[] = NULL;

    foreach ($user->get($valuesId)->referencedEntities() as $e) {
      $reqsList[] = $e->tid->value;
    }

    $completedList =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($taxId);
    
    foreach ($completedList as $cl) {
      if (in_array($cl->tid, $reqsList)){
        $checked = true;
      }
      else {
        $checked = false;
      }
      $r[] = array(
        'name' => $cl->name,
        'checked' => $checked,
      );
    }

    return $r;
  }

}
