<?php

namespace Drupal\drn_i_am_ready\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Url;

/**
 * Class IARRedirect.
 */
class IARRedirect implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public function redirectToUserIARPage(GetResponseEvent $event) {
    $path = $event->getRequest()->getRequestUri();

    if ($path === "/i-am-ready") {
      $uid = \Drupal::currentUser()->id();
      $user_iar_path = "/user/" . $uid . "/i-am-ready";

      $response = new RedirectResponse($user_iar_path, 302);
      $response->send();
    }
  }
      
  /**
  * {@inheritdoc}
  */
  public static function getSubscribedEvents() {
    return [KernelEvents::REQUEST => [['redirectToUserIARPage']]];
  }
}
