window.addEventListener("load", function(){
    // Modify dropdown on Platon themed pages
    const dropdownDiv = document.getElementsByClassName("user-block ml-3 dropdown")[0]
    if (dropdownDiv) {
        let dropdownItems = dropdownDiv.getElementsByClassName("dropdown-item")
        for (const [key, value] of Object.entries(dropdownItems)) {
            const span = value.getElementsByTagName("span")[0]
            if (span) {
                if (span.textContent === "User profile") {
                    span.textContent = "My Dashboard"
                }
                else if (span.textContent === "Settings") {
                    span.textContent = "Update Profile"
                }
                else if (span.textContent === "About" || span.textContent === "Help") {
                    value.style.display = "none"
                }
            }
        }
    }
});
