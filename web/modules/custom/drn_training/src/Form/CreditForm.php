<?php

/**
 * @file
 * Contains \Drupal\drn_training\Form\CreditForm.
 */

namespace Drupal\drn_training\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Credit form.
 */
class CreditForm extends FormBase {
    /**
    * {@inheritdoc}
    */
    public function getFormId() {
        return 'credit_form';
    }

    /**
    * {@inheritdoc}
    */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $database = \Drupal::database();


        /* --- User --- */
        $query_users = $database->query(
            "SELECT
                u.uid i, 
                fn.field_user_first_name_value first_name,
                ln.field_user_last_name_value last_name,
                u.name username, 
                u.mail email
            FROM
                users_field_data u,
                user__field_user_first_name fn,
                user__field_user_last_name ln
            WHERE
                fn.entity_id = u.uid AND
                ln.entity_id = u.uid AND
                ln.bundle = 'user' AND
                fn.bundle = 'user' AND 
                u.status = 1
            ORDER BY
                fn.field_user_first_name_value,
                ln.field_user_last_name_value ASC"
        );
        $result_users = $query_users->fetchAll();

        $options_users = [];
        foreach ($result_users as $ru) {
            $options_users[$ru->i] =  $ru->first_name . " " . $ru->last_name . " (Username: " . $ru->username . " || User ID: " . $ru->i . ")";
        }

        $form['user'] = array(
            '#type' => 'select',
            '#title' => ('User'),
            '#options' => $options_users,
            '#required' => TRUE,
        );

        //if have UID in query string, set default value for user
        $uid = \Drupal::request()->query->get('uid');
        if ($uid) {
            $form["user"]["#default_value"] = $uid;
        }

        /* --- Training --- */
        $query = $database->query(
            "SELECT
                gid.field_learning_path_course_id_value - 10000 i,
                gname.label l
            FROM 
                group__field_learning_path_course_id gid JOIN groups_field_data gname ON gid.entity_id = gname.id
            WHERE
                type = 'learning_path' AND
                gid.field_learning_path_course_id_value >= 10000
            ORDER BY
                gname.label"
        );
        $result = $query->fetchAll();

        $options = [];
        foreach ($result as $r) {
            $options[$r->i] =  $r->l . " (Course #" . $r->i . ")";
        }

        $form['training'] = array(
            '#type' => 'select',
            '#title' => t('Training'),
            '#options' => $options,
            '#required' => TRUE,
        );

        /* --- Additional Fields --- */
        $form['date'] = array(
            '#type' => 'date',
            '#title' => t('Date'),
            '#required' => TRUE,
        );

        $form['notes'] = array(
            '#type' => 'textarea',
            '#title' => t('Notes'),
        );

        /* --- Button(s) --- */
        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Submit'),
        );

        return $form;
    }

    /**
    * {@inheritdoc}
    */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $user_id = $form_state->getValue('user');
        $class_id = $form_state->getValue('training');
        $completed_date = date("Y/m/d", strtotime($form_state->getValue('date')));
        $admin = \Drupal::currentUser()->getUsername();
        $notes = $form_state->getValue('notes');
        
        try {
            $database = \Drupal::database();
            $database->query("call grant_manual_class_credit($user_id,$class_id,'$completed_date','$admin','$notes');");

            $account = \Drupal\user\Entity\User::load($form_state->getValue("user"));
            $name = $account->field_user_first_name->value . " " . $account->field_user_last_name->value;
    
            \Drupal::messenger()->addMessage(t("@user has been given credit for @training", array("@user" => $name, "@training" => $form_state->getValue("training"))));
        }
        catch(Exception $e) {
            \Drupal::logger('drn_training')->error($e->getMessage());
            \Drupal::messenger()->addMessage(t("There was a problem with your request"));
        }

        $url = Url::fromUri('internal:/user/' . $user_id . '/edit#edit-admin-group');
        $form_state->setRedirectUrl($url);
    }
}
