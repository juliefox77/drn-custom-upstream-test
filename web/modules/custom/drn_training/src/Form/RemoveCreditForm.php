<?php

/**
 * @file
 * Contains \Drupal\drn_training\Form\RemoveCreditForm.
 */

namespace Drupal\drn_training\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Credit form.
 */
class RemoveCreditForm extends FormBase {
    /**
    * {@inheritdoc}
    */
    public function getFormId() {
        return 'remove_credit_form';
    }

    /**
    * {@inheritdoc}
    */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $form['user'] = array(
            '#type' => 'hidden',
            '#required' => TRUE,
        );

        $form['training'] = array(
            '#type' => 'hidden',
            '#required' => TRUE,
        );

        $form['date'] = array(
            '#type' => 'hidden',
            '#required' => TRUE,
        );

        $form['notes'] = array(
            '#type' => 'textarea',
            '#title' => t('Reason for removal'),
            '#required' => TRUE,
        );
        
        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Remove Manual Credit'),
        );

        // Set default values from query strings
        $uid = \Drupal::request()->query->get('uid');
        $tid = \Drupal::request()->query->get('tid');
        $date = \Drupal::request()->query->get('date');
        if ($uid) {
            $form["user"]["#default_value"] = $uid;
        }
        if ($tid) {
            $form["training"]["#default_value"] = $tid;
        }
        if ($date) {
            $form["date"]["#default_value"] = $date;
        }

        return $form;
    }

    /**
    * {@inheritdoc}
    */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $user_id = $form_state->getValue('user');
        $class_id = $form_state->getValue('training');
        $completed_date = date("Y/m/d", strtotime($form_state->getValue('date')));
        $admin = \Drupal::currentUser()->getUsername();
        $notes = $form_state->getValue('notes');
        
        try {
            $database = \Drupal::database();
            $database->query("call remove_manual_class_credit($user_id, $class_id, '$completed_date', '$admin', '$notes');");

            $account = \Drupal\user\Entity\User::load($form_state->getValue("user"));
            $name = $account->field_user_first_name->value . " " . $account->field_user_last_name->value;
    
            \Drupal::messenger()->addMessage(t("Credit for @training has been removed from @user", array("@training" => $form_state->getValue("training"), "@user" => $name)));
        }
        catch(Exception $e) {
            \Drupal::logger('drn_training')->error($e->getMessage());
            \Drupal::messenger()->addMessage(t("There was a problem with your request"));
        }
        
        $url = Url::fromUri('internal:/user/' . $user_id . '/edit#edit-admin-group');
        $form_state->setRedirectUrl($url);
    }
}
