window.onload = function (){
	/* JFox 10/2/2021 - removed this code becasue we handle it in the drupal hooks instead. 
	Also, for some users have both New User and Volunteer so we don't want this message here */
	// Display message for users with role new_user
	/*if (document.body.classList.contains('new-user-role')) {
		var titleBlock = document.getElementById('block-editprofile')
		var message = document.createElement("H3")
		message.innerHTML = "Please fill out ALL required information on all tabs to access training courses and registration. Thank you for updating your profile information."
		message.style.color = "red"
		message.style.marginBottom = "25px"
		titleBlock.appendChild(message)
	}*/

/*
	<article class="container-fluid">
	<section class="row px-2 d-none d-lg-flex">
		<article class="col-4">
			<h5>Volunteer Response Roles</h5>
		</article>
		<article class="col-3 text-center">
			<h5>Badge</h5>
		</article>
		<article class="col-3 text-center">
			<h5>Certified</h5>
		</article>
		<article class="col-2 text-center">
			<h5>Deployable</h5>
		</article>
	</section>
	{% for rr in user_response_roles_list %}
		<section class="row px-2 pt-3 banded-row {{ loop.index % 2 == 1 ? "dark" : "light" }} {{ loop.index == 1 ? "first" : "" }}">
			<article class="col-12 col-lg-4 align-self-center text-center text-lg-left">
				<p><a href="{{ rr.link }}">{{ rr.name }} ({{ rr.acronym }})</a></p>
			</article>
			<article class="col-12 col-lg-3 d-none d-lg-flex align-self-center justify-content-center text-center">
				<p>{{ rr.badge }}</p>
			</article>
			<article class="col col-lg-3 d-none d-lg-flex align-self-center justify-content-center text-center">
				{% if rr.completed %}
					<p>Completed</p>
				{% elseif rr.inprog %}
					<p>In Progress</p>
				{% else %}
					<p>Not Started</p>
				{% endif %}
			</article>
			<article class="col col-lg-2 align-self-center justify-content-center text-center">
				<p>
					<span class="d-lg-none">Deployable: </span>
					{% if rr.completed %}
						✓
					{% else %}
						⦸
					{% endif %}
				</p>
			</article>
		</section>
	{% endfor %}
</article>
*/

	// User profile edit page
	if (document.body.classList.contains('page-user-edit') && !document.body.classList.contains('page-user-view')) {
		// Availability options styling
		var aDiv = document.getElementById('edit-field-availability')

		var aOptions = aDiv.getElementsByClassName('form-item')
		
		var aContainer = document.createElement("article")
		aContainer.classList.add('container-fluid')

		var aHeaderRow = document.createElement("section")
		aHeaderRow.classList.add('row')
		aHeaderRow.classList.add('px-2')
		aHeaderRow.innerHTML =
			'<article class="col"></article>' +
			'<article class="col text-center text-wrap-anywhere">' +
				'<h5 class="d-none d-md-block">Morning</h5>' +
				'<h5 class="d-md-none">Morn</h5>' +
			'</article>' +
			'<article class="col text-center text-wrap-anywhere">' +
				'<h5 class="d-none d-md-block">Afternoon</h5>' +
				'<h5 class="d-md-none">Noon</h5>' +
			'</article>' +
			'<article class="col text-center text-wrap-anywhere">' +
				'<h5 class="d-none d-md-block">Evening</h5>' +
				'<h5 class="d-md-none">Eve</h5>' +
			'</article>'
		aContainer.appendChild(aHeaderRow)

		var rows = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
		var mobileRows = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat']
		for (var i = 0; i < rows.length; i++) {
			var row = document.createElement("section")
			row.classList.add('row')
			row.classList.add('px-2')
			row.classList.add('pt-3')
			row.classList.add('banded-row')
			if (i % 2 === 0)
				row.classList.add('dark')
			else
				row.classList.add('light')
			if (i === 0)
				row.classList.add('first')
			row.innerHTML = '<article class="col text-wrap-anywhere"><p class="d-none d-md-block"><strong>' + rows[i] + '</strong></p><p class="d-md-none"><strong>' + mobileRows[i] + '</strong></p></article>'
			aContainer.appendChild(row)
			for (var j = 0; j < 3; j++) {
				var col = document.createElement("article")
				col.classList.add('col')
				col.classList.add('text-center')
				col.appendChild(aOptions[(i * 3) + j].getElementsByTagName('input')[0])
				row.appendChild(col)
			}
		}

		// Hide availability option labels
		var labels = document.getElementById('edit-field-availability').getElementsByTagName('label')
		for (var i = 0; i < labels.length; i++) {
			labels[i].classList.add('d-none')
		}

		aDiv.appendChild(aContainer)

		// Move description above table
		var divFieldsetWrapper = document.getElementById('edit-field-availability--wrapper').getElementsByClassName('fieldset-wrapper')[0]
		divFieldsetWrapper.appendChild(aDiv)

		// Change page title
		const pageTitle = document.querySelectorAll("[id$=-editprofile]")[0].getElementsByTagName("h2")[0]
		if (pageTitle)
			if(document.getElementById("edit-field-user-preferred-name-0-value").value){
				pageTitle.innerHTML = "Update Profile: " + document.getElementById("edit-field-user-first-name-0-value").value + " (" + document.getElementById("edit-field-user-preferred-name-0-value").value + ") " + document.getElementById("edit-field-user-last-name-0-value").value
			} else {
				pageTitle.innerHTML = "Update Profile: " + document.getElementById("edit-field-user-first-name-0-value").value + " " + document.getElementById("edit-field-user-last-name-0-value").value
			}
	}

	// Change Update Profile link to href from avatar dropdown
	const sideMenu = document.getElementById('sidebar-nav')
	if (sideMenu) {
		const profileLink = sideMenu.getElementsByClassName('update profile')[0]
		const editProfileURL = document.getElementById('header-user').getElementsByClassName('dropdown-menu-content')[0].getElementsByClassName('dropdown-item')[1].getAttribute("href")
		if (profileLink) {
			profileLink.href = editProfileURL
		}
		else
			console.log("Edit profile link is not present in sidebar menu")
		// Change I AM Ready Program link using id from avatar dropdown
		const iarLink = sideMenu.getElementsByClassName('i am ready program')[0]
		if (iarLink) {
			iarLink.href = "/user/" + editProfileURL.match(/\/user\/(\d+)\/edit/)[1] + "/i-am-ready"
		}
		else
			console.log("I AM Ready Program link is not present in sidebar menu")
	}

	/* Fill out payment info with anonymous data so that the checkout process can be completed
	if (document.body.classList.contains('section-checkout')) {
		let givenName = document.getElementsByClassName("given-name required")[0]
		if (givenName)
			givenName.value = "N/A"
		let familyName = document.getElementsByClassName("family-name required")[0]
		if (familyName)
			familyName.value = "N/A"
		let address = document.getElementsByClassName("address-line1 required")[0]
		if (address)
			address.value = "N/A"
		let locality = document.getElementsByClassName("locality required")[0]
		if (locality)
			locality.value = "N/A"
		let state = document.getElementsByClassName("administrative-area required")[0]
		if (state)
			state.value = "TX"
		let zipCode = document.getElementsByClassName("postal-code required")[0]
		if (zipCode)
			zipCode.value = "78787"
		let saveAddress = document.getElementById("edit-payment-information-billing-information-copy-to-address-book")
		if (saveAddress)
			saveAddress.checked = ""
	}*/
}

jQuery(window).on("load resize",function(e){
    if (window.innerWidth > 1024){
		return false;
	}else{
		var thead = [];
		jQuery('.view-scheduled-trainings-for-course table thead th').each(function(){
			thead.push(jQuery(this).text());
		})
		jQuery('.view-scheduled-trainings-for-course table tbody tr').each(function(){			
			jQuery(this).find('td').each (function(i,el) {
				if(thead[i] != ''){
					jQuery(this).attr("label",thead[i]+' :');
				}
			})
		})
	}
});