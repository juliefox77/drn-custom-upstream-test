<?php

/**
 * @file
 * ADRN theme settings.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * Form override for theme settings.
 */
function drn_base_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['drn_settings']["dashboard_notice_group"] = [
    '#type' => 'details',
    '#title' => t('Dashboard Notice'),
    '#open' => TRUE,
  ];
  $form['drn_settings']["dashboard_notice_group"]["dashboard_notice_enabled"] = [
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => theme_get_setting('dashboard_notice_enabled'),
  ];
  $form['drn_settings']["dashboard_notice_group"]["dashboard_notice_text"] = [
    '#type' => 'textarea',
    '#title' => t('Display Text'),
    '#default_value' => theme_get_setting('dashboard_notice_text'),
  ];
}



function drn_base_theme_preprocess(&$variables, $hook) {
    $variables['base_path'] = base_path();
}